using System;
using System.Collections.Generic;
using System.Linq;

namespace UML
{
    class Program
    {
        private static Book[] _books = {
                new Book( 1, "Бесы", "Ф. М. Достоевский", "", 1872, 768 ),
                new Book( 2, "Граф Монте-Кристо. Том 1", "Александр Дюма", "Правда", 1983, 689)
        };

        private static LibraryCard[] _libraryCards = {
                new LibraryCard( 101, "Вася", "Студент", new Dictionary<Book, DateTime>()),
                new LibraryCard( 102, "Петя", "Студент", new Dictionary<Book, DateTime>())
        };

        static void Main(string[] args)
        {
            GiveBook();
            ReturnBook();
        }

        private static void GiveBook() 
        {
            SearchEngine searchEngine = new SearchEngine(_libraryCards, _books);

            string[] requiredBooksTitles = new string[] { "Бесы" };

            searchEngine.RequestCatalog();

            searchEngine.RequestLCard(_libraryCards[1].ID);

            foreach (string requiredBookTitle in requiredBooksTitles)
            {
                searchEngine.RequestBook(requiredBookTitle);
            }

            Console.ReadKey();
        }

        private static void ReturnBook()
        {
            Book[] books_to_return = {
                new Book(1, "Улисс", "Джеймс Джойс", "", 1922, 810)
            };

            SearchEngine searchEngine = new SearchEngine(_libraryCards, _books);

            searchEngine.RequestLCard(_libraryCards[1].ID);

            foreach (var book_to_return in books_to_return)
            {
                searchEngine.TakeBookBack(book_to_return);
            }

            Console.ReadKey();
        }
    }

    class SearchEngine 
    {
        private LCard_List _cards_List;
        private Books_List _books_List;

        private int _clientID = 0;

        public SearchEngine(LibraryCard[] libraryCards, Book[] books) 
        {
            _cards_List = new LCard_List(libraryCards);

            _books_List = new Books_List(books);
        }

        public void RequestCatalog() 
        {
            Console.WriteLine("Был вызван метод запроса каталога (RequestCatalog)\n");

            BooksCatalog catalog = new BooksCatalog(_books_List);

            catalog.Print();
        }

        public LibraryCard RequestLCard(int id)
        {
            Console.WriteLine("Был вызван метод запроса читательского билета (RequestLCard)\n");

            _clientID = id;
            return _cards_List.GetLCard(id);
        }

        public Book RequestBook(string title) 
        {
            Console.WriteLine("Был вызван метод запроса книги (RequestBook)\n");

            if (_clientID > 0)
            {
                Book book = _books_List.GetBook(title);
                _cards_List.AddTakenBook(_clientID, book);

                return book;
            }
            else
            {
                throw new Exception("Информация по читательскому билету не была проверена!!!");
            }
        }

        public void TakeBookBack(Book book) 
        {
            Console.WriteLine("Был вызван метод возврата книги (TakeBookBack)\n");

            if (_clientID > 0)
            {
                _books_List.AddBook(book);
                _cards_List.RemoveTakenBook(_clientID, book);
            }
            else
            {
                throw new Exception("Информация по читательскому билету не была проверена!!!");
            }
        }
    }

    class LCard_List
    {
        private Dictionary<int, LibraryCard> _lCards = new Dictionary<int, LibraryCard>();

        public LCard_List(LibraryCard[] lCards)
        {
            AddToDictionary(lCards);
        }

        private void AddToDictionary(LibraryCard[] lCards)
        {
            foreach (var lCard in lCards)
            {
                _lCards[lCard.ID] = lCard;
            }
        }
        public LibraryCard GetLCard(int clientID)
        {
            Console.WriteLine("Вызов метода получения читательского билета (GetLCard)\n");
            return _lCards[clientID];
        }

        public void AddTakenBook(int id, Book book) 
        {
            Console.WriteLine("Вызов метода внесения книги в список выданных (AddTakenBook)\n");
            _lCards[id].AddDebt(book);
        }

        public void RemoveTakenBook(int id, Book book) 
        {
            Console.WriteLine("Вызов метода удаления книги из списа выданных (RemoveTakenBook)\n");
            _lCards[id].RemoveDebt(book);
        }
    }

    class Books_List 
    {
        private Dictionary<string, Book> _booksDic = new Dictionary<string, Book>();

        public Books_List(Book[] books)
        {
            AddToDictionary(books);
        }

        private void AddToDictionary(Book[] books)
        {
            foreach (var book in books)
            {
                _booksDic[book.Title] = book;
            }
        }

        public Book[] GetAllBooks() 
        {
            Console.WriteLine("Вызов метода получения всех книг в виде массива экземпляров (GetAllBooks)\n");

            Dictionary<string, Book> booksNotTaken = _booksDic;

            foreach (var book in booksNotTaken)
            {
                if (book.Value.Taken)
                {
                    booksNotTaken.Remove(book.Key);
                }
            }

            return booksNotTaken.Values.ToArray();
        }

        public Book GetBook(string title) 
        {
            Console.WriteLine("Вызов метода получения книги (GetBook)\n");

            foreach (var book in _booksDic)
            {
                if (book.Key == title && book.Value.Taken)
                {
                    throw new Exception($"Book {title} has already been taken!!!");
                }
            }

            _booksDic[title].Taken = true;
            return _booksDic[title];
        }

        public void AddBook(Book book) 
        {
            Console.WriteLine("Вызов метода внесения книги (AddBook)\n");

            book.Taken = false;
            _booksDic[book.Title] = book;
        }
    }

    class BooksCatalog
    {
        private Book[] _books;

        public BooksCatalog(Books_List list)
        {
            Console.WriteLine("Создание каталога\n");

            SetAllBooks(list);
        }

        private void SetAllBooks(Books_List list) 
        {
            _books = list.GetAllBooks();
        }

        public void Print() 
        {
            Console.WriteLine("Каталог:");
            foreach (var book in _books)
            {
                Console.WriteLine($"  {book.Title} от {book.Author}.\n  Издательство {book.Publication} {book.Year}.\n");
            }
        }
    }

    class LibraryCard
    {
        private string _name;
        private string _employment;
        private Dictionary<Book, DateTime> _debts;
        
        private int _id;

        public LibraryCard(int id, string name, string employment, Dictionary<Book, DateTime> debts)
        {
            _id = id;
            _name = name;
            _employment = employment;
            _debts = debts;
        }

        public int ID 
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }

        public string Employment
        {
            get { return _employment; }
        }

        public void PrintDebts()
        {
            foreach (var debt in _debts)
            {
                Console.WriteLine($"{debt.Key.Title}. Взята {debt.Value.ToString("d")}");
            }
        }

        public void AddDebt(Book book) 
        {
            Console.WriteLine("Вызов метода внесения выданной книги в читательский билет (AddDebt)");
            _debts.Add(book, DateTime.UtcNow);
        }

        public void RemoveDebt(Book book)
        {
            Console.WriteLine("Вызов метода удаления выданной книги из читательского билета (RemoveDebt)");
            _debts.Remove(book);
        }
    }

    class Book
    {
        private string _author;
        private string _publication;
        private int _year;
        private int _pagesCount;
        private bool _taken = false;

        private int _id;
        private string _title;

        public Book(int id, string title, string author, string publication, int year, int pagesCount)
        {
            _id = id;
            _title = title;
            _author = author;
            _publication = publication;
            _year = year;
            _pagesCount = pagesCount;
        }

        public int ID
        {
            get { return _id; }
        }

        public string Title 
        {
            get { return _title; }
        }

        public string Author
        {
            get { return _author; }
        }

        public string Publication 
        {
            get { return _publication; }
        }

        public int Year 
        {
            get { return _year; }
        }

        public int PagesCount 
        {
            get { return _pagesCount; }
        }

        public bool Taken
        {
            get { return _taken; }
            set { Console.WriteLine("Присвоение значения `взята` или `в наличии` книге.\n"); _taken = value; }
        }
    }
}
